<?php

return [
    'timeout'       => (int) env('BROADCAST_TERMINAL_TIMEOUT', 3600),
    'idle-timeout'  => (int) env('BROADCAST_TERMINAL_IDLE_TIMEOUT', 60),
    'ssh' => [
        'user' => env('BROADCAST_TERMINAL_SSH_USER', 'www-data'),
        'host' => env('BROADCAST_TERMINAL_SSH_HOST', '127.0.0.1'),
        'port' => (int) env('BROADCAST_TERMINAL_SSH_PORT', 22),
    ],
];
