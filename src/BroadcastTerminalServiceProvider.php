<?php

namespace Shizzen\BroadcastTerminal;

use Illuminate\Support\ServiceProvider;

class BroadcastTerminalServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/broadcast-terminal.php', 'broadcast-terminal'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->commands([
            Commands\BroadcastCommand::class,
            Commands\BroadcastFileCommand::class,
        ]);

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/broadcast-terminal.php' => config_path('broadcast-terminal.php'),
            ], 'broadcast-terminal-config');
        }
    }
}
