<?php

namespace Shizzen\BroadcastTerminal\Commands;

use Symfony\Component\{
    Process\Process,
    Console\Input\InputOption,
    Console\Input\InputArgument
};
use Illuminate\Console\Command;
use Shizzen\BroadcastTerminal\Traits\HasBroadcastCallback;

abstract class AbstractBroadcastCommand extends Command
{
    use HasBroadcastCallback;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        [
            'timeout'       => $timeout,
            'idle-timeout'  => $idleTimeout,
            'host'          => $host,
            'user'          => $user,
            'port'          => $port,
            'public'        => $public,
            'event'         => $event,
        ] = $this->options();

        $command = $this->getCommand();

        if (!in_array($host, ['127.0.0.1', 'localhost'])) {
            $command = sprintf(
                'ssh -p %d %s@%s %s',
                $port,
                $user,
                $host,
                $command
            );
        }

        return Process::fromShellCommandLine($command)
            ->setTimeout((int) $timeout)
            ->setIdleTimeout((int) $idleTimeout)
            ->mustRun(
                static::getBroadcastCallback($public, $this->argument('channel'), $event)
            )
            ->getExitCode();
    }

    /**
     * Get the command to run.
     *
     * @return string
     */
    abstract protected function getCommand();

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['channel', InputArgument::REQUIRED, 'Broadcasting channel name'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        [
            'timeout'       => $timeout,
            'idle-timeout'  => $idleTimeout,
            'ssh'           => $ssh,
        ] = config('broadcast-terminal');

        return [
            ['timeout', 't', InputOption::VALUE_OPTIONAL, 'The command timeout (seconds)', $timeout],
            ['idle-timeout', 'T', InputOption::VALUE_OPTIONAL, 'The command idle timeout (seconds)', $idleTimeout],
            ['user', 'u', InputOption::VALUE_OPTIONAL, 'SSH user', $ssh['user']],
            ['host', 'H', InputOption::VALUE_OPTIONAL, 'SSH host', $ssh['host']],
            ['port', 'p', InputOption::VALUE_OPTIONAL, 'SSH port', $ssh['port']],
            ['public', 'P', InputOption::VALUE_NONE, 'Broadcast onto a public channel instead of private'],
            ['event', 'E', InputOption::VALUE_OPTIONAL, 'Broadcast event name', $this->eventName ?? $this->name],
        ];
    }
}
