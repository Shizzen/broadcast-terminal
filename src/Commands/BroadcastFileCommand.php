<?php

namespace Shizzen\BroadcastTerminal\Commands;

use Symfony\Component\Console\Input\{
    InputOption,
    InputArgument
};

class BroadcastFileCommand extends AbstractBroadcastCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'broadcast:file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Broadcast file content on real-time';

    /**
     * Get the command to run.
     *
     * @return string
     */
    protected function getCommand()
    {
        return sprintf(
            'tail -f -n %d %s',
            $this->option('lines'),
            $this->argument('file')
        );
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array_merge(
            parent::getArguments(),
            [
                ['file', InputArgument::REQUIRED, 'Path of the file to broadcast'],
            ]
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array_merge(
            parent::getOptions(),
            [
                ['lines', 'l', InputOption::VALUE_OPTIONAL, 'The lines number to  print immediately', 0],
            ]
        );
    }
}
