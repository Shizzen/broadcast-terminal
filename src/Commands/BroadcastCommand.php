<?php

namespace Shizzen\BroadcastTerminal\Commands;

use Symfony\Component\Console\Input\InputArgument;

class BroadcastCommand extends AbstractBroadcastCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'broadcast:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Broadcast command output on real-time';

    /**
     * Get the command to run.
     *
     * @return string
     */
    protected function getCommand()
    {
        return implode(' ', $this->argument('cmd'));
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array_merge(
            parent::getArguments(),
            [
                ['cmd', InputArgument::IS_ARRAY, 'The command whose output is broadcasted'],
            ]
        );
    }
}
