<?php

namespace Shizzen\BroadcastTerminal\Traits;

use Shizzen\BroadcastTerminal\Events\BroadcastedData;

trait HasBroadcastCallback
{
    /**
     * Get broadcast callback.
     *
     * @param  bool  $public
     * @param  string  $channel
     * @param  string  $event
     * @return callable
     */
    protected static function getBroadcastCallback(bool $public, string $channel, string $event)
    {
        return function($type, $buffer) use ($public, $channel, $event) {
            event(new BroadcastedData(
                $public, $channel, $event, $buffer
            ));
        };
    }
}
