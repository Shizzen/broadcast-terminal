<?php

namespace Shizzen\BroadcastTerminal\Events;

use Illuminate\Broadcasting\{
    Channel,
    PrivateChannel,
    InteractsWithSockets
};
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BroadcastedData implements ShouldBroadcast
{
    use SerializesModels, InteractsWithSockets;

    /**
     * The broadcast channel class.
     *
     * @var string
     */
    protected $channelType;

    /**
     * The broadcast channel name.
     *
     * @var string
     */
    protected $channelName;

    /**
     * The broadcast event name.
     *
     * @var string
     */
    protected $eventName;

    /**
     * The data to broadcast.
     *
     * @var mixed
     */
    public $data;

    /**
     * Create a new event instance.
     *
     * @param  bool  $public
     * @param  string  $channelName
     * @param  string  $eventName
     * @param  mixed  $data
     */
    public function __construct(bool $public, string $channelName, string $eventName, $data)
    {
        $this->channelType = $public ? Channel::class : PrivateChannel::class;
        $this->channelName = $channelName;
        $this->eventName = $eventName;
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        $channelType = $this->channelType;
        return new $channelType($this->channelName);
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return $this->eventName;
    }
}
